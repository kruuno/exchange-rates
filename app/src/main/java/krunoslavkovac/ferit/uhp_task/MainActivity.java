package krunoslavkovac.ferit.uhp_task;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    Button button;
    TextView resultBuy;
    Spinner spinner_from;
    private ProgressDialog dialog;

    Double valueBuyRate, valueBuyRate2, rez;

    List<String> allCodes = new ArrayList<String>();
    List<String> buyingRates = new ArrayList<String>();
    List<String> sellingRates = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button) findViewById(R.id.button);
        resultBuy = (TextView) findViewById(R.id.resultBuy);

        dialog = new ProgressDialog(MainActivity.this);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                convertBuyingRates();
            }
        });

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new ReadJSON().execute("http://hnbex.eu/api/v1/rates/daily/?date=YYYY-MM-DD");
            }
        });
    }

    private void setSpinnerFrom() {
        spinner_from = (Spinner) findViewById(R.id.spinner_from);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item ,allCodes);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_from.setAdapter(dataAdapter);

        spinner_from.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {
                Toast.makeText(MainActivity.this,parent.getItemAtPosition(position)+" selected" , Toast.LENGTH_LONG).show();
                valueBuyRate = Double.parseDouble(buyingRates.get(position));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setSpinnerTo() {
        spinner_from = (Spinner) findViewById(R.id.spinner_to);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item ,allCodes);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_from.setAdapter(dataAdapter);

        spinner_from.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {
                Toast.makeText(MainActivity.this,parent.getItemAtPosition(position)+" selected" , Toast.LENGTH_LONG).show();
                valueBuyRate2 = Double.parseDouble(buyingRates.get(position));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public void convertBuyingRates(){
        rez = valueBuyRate/valueBuyRate2;
        resultBuy.setText(Double.toString(rez).format("%.5f", rez));
    }


    class ReadJSON extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Geting exchange rates...");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            return readURL(params[0]);
        }

        @Override
        protected void onPostExecute(String content) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            try {
                JSONArray jsonArray =  new JSONArray(content);

                for(int i =0;i<jsonArray.length(); i++){
                    JSONObject productObject = jsonArray.getJSONObject(i);
                    String code = productObject.getString("currency_code");
                    String bRates = productObject.getString("buying_rate");
                    String sRates = productObject.getString("selling_rate");


                    allCodes.add(code);
                    setSpinnerFrom();
                    setSpinnerTo();

                    buyingRates.add(bRates);
                    sellingRates.add(sRates);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private static String readURL(String theUrl) {
        StringBuilder content = new StringBuilder();
        try {
            // create a url object
            URL url = new URL(theUrl);
            // create a urlconnection object
            URLConnection urlConnection = url.openConnection();
            // wrap the urlconnection in a bufferedreader
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            String line;
            // read from the urlconnection via the bufferedreader
            while ((line = bufferedReader.readLine()) != null) {
                content.append(line + "\n");
            }
            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return content.toString();
    }
}